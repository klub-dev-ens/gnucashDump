BIN = gnucashdump
COMP = ocamlbuild -use-ocamlfind
SRC = data/test.xml

compile:
	$(COMP) $(BIN).native
	mv $(BIN).native $(BIN)

doc:
	$(COMP) -docflags -charset,utf-8 doc.docdir/index.html

test: compile
	./$(BIN) --html html_full $(SRC)
	./$(BIN) --restrict Comptalbatros --html html_restricted $(SRC)
	@echo "Compta complète dans le dossier html_full/"
	@echo "Compta restreinte à une famille de comptes dans html_restricted/"

clean:
	$(COMP) -clean
