# gnucashDump

Programme pour dumper tout ou partie de la compta du logiciel gnucash.

## Dépendances

Les programmes ocamlbuild et ocamlfind doivent être installés sur le système 
ainsi que les bibliothèques ocaml `xml-light` et `sqlite3`. Pour tout mettre à
jour à l'aide d'[opam](https://opam.ocaml.org/doc/Install.html) :

    opam update && opam install ocamlbuild ocamlfind xml-light sqlite3

## Comment ça marche

Cloner puis compiler le projet en tapant la commande `make` dans le dossier
racine. La commande `make test` donne un exemple d'untilisation sur une compta 
test.

Le fichier d'entrée doit être un xml produit par gnucash. Par défaut, gnucash
enregistre en xml gzippé. Pour rendre le fichier exploitable, exécuter :

    mv compta.gnucash compta.gz && gunzip compta.gz

Pour dumper au format souhaité, entrer l'une des commandes suivantes :

    gnucashdump --html "dossier" compta
    gnucashdump --sqlite "dbname" compta
    gnucashdump --html "dossier" --sqlite "dbname" compta
    
il est possible de restreindre à l'ensemble des sous-comptes d'un compte
`mon_compte`. Pour cela, ajouter l'option `--restrict mon_compte`.
   
## plus de documentation

La commande `make doc` produit une brève documentation accessible dans le
dossier pointé par `doc.docdir`.

