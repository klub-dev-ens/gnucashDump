(** Command line options, see 'options' below *)
let restricted = ref ""
let html = ref ""
let sqlite = ref ""

(** The input file *)
let ifile = ref ""

let set_str r s = r := s

(** Command line options *)
let options = 
  [ "--html", Arg.String (set_str html),
    "<file>  Enable HTML output and set the output folder.";

    "--sqlite", Arg.String (set_str sqlite),
    "<file>  Enable Sqlite3 dump ans set the output database name.";

    "--restrict", Arg.String (set_str restricted),
    "<name>  Dump only account <name> and its children." 
  ]

let usage = "usage: gnucashdump [--html <file>] [--sqlite <file>] input_file"
let err_usage () = Arg.usage options usage; exit 1

let () =
  (* Parsing the command line *)
  Arg.parse options (set_str ifile) usage;
  if !ifile = "" then begin
    Printf.eprintf "No input file provided.\n";
    err_usage ()
  end;
  if not (Sys.file_exists !ifile) then begin
    Printf.eprintf "File %s doesn't exist.\n" !ifile;
    exit 1
  end;
  if !html="" && !sqlite="" then begin
    Printf.eprintf "Not output specified.\n";
    err_usage ()
  end;
  (* Parsing of the input file *)
  let cpt = Reader.read_file !ifile in
  (* Computing restriction if needed *)
  let cpt = if !restricted <> "" then
    Management.restrict_by_name !restricted cpt
  else cpt in
  (* Writing the result *)
  if !html <> "" then Writer.write_html !html cpt;
  if !sqlite <> "" then Writer.write_sqlite !sqlite cpt;
  exit 0
