(** Operations over gnucash data *)

open Types

let restrict_by_name name cpt =
  let rem_tree, deads = Tree.pick (fun act -> act.name=name) cpt.accounts in
  let rem_acts = Hashtbl.create 17 in
  Tree.iter (fun act -> Hashtbl.add rem_acts act.act_guid ()) rem_tree;
  let rem_transactions =
    let check tr = List.exists (fun (guid, _, _) ->
      Hashtbl.mem rem_acts guid) tr.splits in
    List.filter check cpt.transactions in
  { accounts = rem_tree ;
    dead_acts = deads ;
    transactions = rem_transactions }

