(** Operations over gnucash data *)

(** Restriction to accounts under a given account referenced by its name *)
val restrict_by_name : string -> Types.compta -> Types.compta
