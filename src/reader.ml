(** Module Reader: parsing of gnucash files. *)

open Xml
open Types

(** {1 Usefull functions } *)

(** Checks wether a xml element is a gnucash account *)
let is_account xml =
  try tag xml = "gnc:account"
  with Not_element _ -> false

(** Insertion of an account in the accounts tree *)
let insert act tree =
  if act.parent = "null" then Tree.singleton act
  else let predicate x = (x.act_guid = act.parent) in
       Tree.insert predicate act tree

(** {1 Gnucash elements parsers } *)

(** Parsing of an accounts in the xml format *)
let read_account xml =
  let rec aux (n,g,p) = function
    | [] -> { name = n ; act_guid = g ; parent = p }
    | (Element ("act:name", _, [cont]))::l -> aux (pcdata cont, g, p) l
    | (Element ("act:id", _, [cont]))::l -> aux (n, pcdata cont, p) l
    | (Element ("act:parent", _, [cont]))::l -> aux (n, g, pcdata cont) l     
    | (Element _)::l | (PCData _)::l -> aux (n, g, p) l in
  aux ("", "", "null") (children xml)

(** Parsing of a sub-transaction in a transaction (split) *)
let read_split split = 
  let rec aux (v, act, memo) = function
    | [] -> (act, v, memo)
    | (Element ("split:value", _, [cont]))::l ->
        let strval = Str.split (Str.regexp "/") (pcdata cont) in
        let value = int_of_string (List.hd strval) in
        aux (value, act, memo) l
    | (Element ("split:account", _, [cont]))::l ->
        aux (v, pcdata cont, memo) l
    | (Element ("split:memo", _, [cont]))::l ->
        aux (v, act, pcdata cont) l
    | (Element _)::l | (PCData _)::l -> aux (v, act, memo) l in
  aux (0, "", "") (children split)

(** Parsing of a transaction *)
let read_transaction xml =
  let rec aux (guid, d, s, desc) = function
    | [] -> { guid = guid ; date = d ; splits = s ; description = desc}
    | (Element ("trn:id", _, [cont]))::l ->
        aux (pcdata cont, d, s, desc) l
    | (Element ("trn:date-posted", _, [cont]))::l ->
        let date = pcdata (List.hd (children cont)) in
        let date = String.sub date 0 10 in
        aux (guid, date, s, desc) l
    | (Element ("trn:description", _, [cont]))::l ->
        aux (guid, d, s, pcdata cont) l
    | (Element ("trn:splits", _, splits))::l ->
        aux (guid, d, List.map read_split splits, desc) l     
    | (Element _)::l | (PCData _)::l -> aux (guid, d, s, desc) l in
  aux ("", "", [], "") (children xml)

(** {1 The different steps in the parsing of the XML file } *)

(** Structure of a gnucash XML file:
- gnc-v2
    - gnc:count-data (metadata)
    - gnc:book
        - metadata (gnc:count-data, gnc:commodity, ...)
        - metadata
        - ...
        - gnc:account 1
        - gnc:account 2
        - ...
        - gnc:transaction 1
        - gnc:transaction 2
        - ...
        - gnc:transaction n
*)

(** Ignoring the metadata at the beginning of the gnucash file *)
let ignore_header xml =
  let l = children (List.nth (children xml) 1) in
  let rec aux = function
    | [] -> assert false
    | x::xs when is_account x -> x::xs
    | _::xs -> aux xs in
  aux l

(** Reading all the accounts.
 * Returns an account tree and the list of transactions (as xml data) *)
let rec get_accounts ?tree:(tree=Tree.Void) = function
  | [] -> assert false
  | x::xs when is_account x ->
      let act = read_account x in
      get_accounts ~tree:(insert act tree) xs
  | x::xs -> tree, (x::xs)

(** Reading the transactions *) 
let read_transactions xmls = 
  let f res = function
    | Element _ as xml when tag xml = "gnc:transaction" ->
        (read_transaction xml)::res
    | Element _ | PCData _ -> res in
  List.fold_left f [] xmls

(** {1 Global reader } *)

let read_file filename =
  (* First step: parsing the (XML) file *)
  let xml = parse_file filename in
  (* Second step: Ignoring metadata *)
  let xml = ignore_header xml in
  (* Third step:
   * - Parsing the accounts data and represent the structure as a tree
   * - The transactions are in the xml list *)
  let accounts, xmls = get_accounts xml in
  (* Last step: parsing the transactions *)
  let transactions = read_transactions xmls in
  { accounts = accounts ; dead_acts = [] ; transactions = transactions }

