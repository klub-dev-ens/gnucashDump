(** {0 Module for parsing gnucash files} *)

(** Reading and parsing of a gnucash file referenced by its name *)
val read_file : string -> Types.compta
