type 'a tree = Void | Node of ('a * 'a tree list)

exception Cannot_insert

let rec iter f = function
  | Void -> ()
  | Node (x, ts) ->
      f x;
      List.iter (iter f) ts

let singleton x = Node (x, [])

let rec insert predicate elt = function
  | Void -> raise Cannot_insert
  | Node (x, ts) ->
      if predicate x then Node (x, (singleton elt)::ts)
      else Node (x, insert_aux predicate elt ts)
and insert_aux p elt = function
  | [] -> raise Cannot_insert
  | t::ts ->
      try (insert p elt t)::ts
      with Cannot_insert -> t::(insert_aux p elt ts)

let rec _as_list l = function
  | Void -> l
  | Node (x, ts) -> x :: (List.fold_left _as_list l ts)
let as_list t = _as_list [] t

let rec _pick predicate deads = function
  | Void -> raise Not_found
  | Node (x, ts) ->
      if predicate x then Node (x, ts), deads
      else pick_aux predicate (x::deads) ts
and pick_aux p deads = function
  | [] -> raise Not_found
  | t::ts -> try
               let (res_t, res_d) = _pick p deads t in
               res_t, List.fold_left _as_list res_d ts
             with Not_found ->
               let deads = _as_list deads t in
               pick_aux p deads ts

let pick p = _pick p []
