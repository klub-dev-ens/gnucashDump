type 'a tree = Void | Node of ('a * 'a tree list)
exception Cannot_insert

val iter : ('a -> unit) -> 'a tree -> unit
val singleton : 'a -> 'a tree
val insert : ('a -> bool) -> 'a -> 'a tree -> 'a tree
val pick : ('a -> bool) -> 'a tree -> 'a tree * 'a list
val as_list : 'a tree -> 'a list
