(** Types of gnucash data *)

(** Gnucash account *)
type account = { name : string ; act_guid : string ; parent : string }

(** Gnucash transaction between two or more accounts *)
type transaction = {
  guid : string ;
  date : string ;
  description : string ;
  splits : (string * int * string) list ;
}

(** The whole book *)
type compta = {
  accounts : account Tree.tree ;
  dead_acts : account list;
  transactions : transaction list
}
