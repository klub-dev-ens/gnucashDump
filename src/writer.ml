(** Module for writing gnucash data *)

open Printf
open Types
open Sqlite3

(** {1 Common usefull functions *)

(** Helps for printing, don't ask me to explain *)
let (%) n k = let ent = n/k in abs(n - ent*k)

(** {1 HTML output } *)

(** {2 Miscellaneous printing functions *)

(** Prints a HTML <head> section in a out_channel with title as an optional
 * argument *)
let header ?title:(title="Compta K-Fêt") oc = 
  fprintf oc "<!DOCTYPE html>\n<html>\n<head>\n";
  fprintf oc "\t<title>%s</title>\n\t<meta charset=\"UTF-8\" />\n" title;
  fprintf oc "</head>\n\n"

(** Prints a given number of tabs *)
let rec tab oc = function
  | 0 -> ()
  | n -> fprintf oc "\t"; tab oc (n-1)

(** {2 Gnucash objects printing } *)

(** A couple of functions writing a account tree as a <ul> list *)
let rec _write_tree oc depth = function
  | Tree.Void -> ()
  | Tree.Node (act, []) ->
      fprintf oc "<a href=\"%s.html\">%s</a>" act.act_guid act.name
  | Tree.Node (act, l) ->
      fprintf oc "<a href=\"%s.html\">%s</a>\n" act.act_guid act.name;
      tab oc depth;
      fprintf oc "<ul>\n";
      let depth = depth + 1 in
      List.iter (fun t' ->
        tab oc depth; fprintf oc "<li>";
        _write_tree oc depth t'; 
        fprintf oc "</li>\n") l;
      tab oc (depth-1); fprintf oc "</ul></li>"
let write_tree oc t = _write_tree oc 0 t; fprintf oc "\n"

(** Prints all the informations contained in a transaction on all concerned
 * accounts pages *)
let write_tr accounts dead_acts tots tr =
  let line oc (date, desc, memo, act, price) =
    fprintf oc "\t<tr><td>%s</td><td>%s</td><td>%s</td>" date desc memo;
    fprintf oc "<td>%s</td><td>%d,%.2d</td></tr>\n"
      act (price/100) (price%100) in
  let rec _mklines guid main = function
    | [] -> main, []
    | (guid', price, memo)::xs ->
        let name = try fst (Hashtbl.find accounts guid')
          with Not_found -> Hashtbl.find dead_acts guid' in
        if guid' = guid then
          _mklines guid (tr.date, tr.description, memo, name, price) xs
        else
          let main', res = _mklines guid main xs in
          main', ("", "", memo, name, price)::(res) in
  let mklines guid l =
    let (date, desc, memo, _, price) as main, res =
      _mklines guid ("", "", "", "", 0) l in
    let prev = Hashtbl.find tots guid in Hashtbl.replace tots guid (prev+price);
    if List.length res = 1 then
      let (_, _, _, act, price) = List.hd res in
      [(date, desc, memo, act, price)]
    else
      main :: res in
  List.iter (fun (guid, _, _) ->
    try begin
      let _, oc = Hashtbl.find accounts guid in
      let lines = mklines guid tr.splits in
      List.iter (line oc) lines
    end with Not_found -> ()) tr.splits

(** The main writer *)
let write_html dirname cpt =
  begin try if not (Sys.is_directory dirname) then
    raise (Invalid_argument ("Not a directory: "^dirname^"."));
  with Sys_error _ -> Unix.mkdir dirname 0o755 end;
  (* Index *)
  let index_oc = open_out (dirname^"/index.html") in
  header index_oc; 
  fprintf index_oc "<body>\n";
  write_tree index_oc cpt.accounts;
  (* Accounts pages *)
  let accounts = Hashtbl.create 17 in
  let dead_acts = Hashtbl.create 17 in
  let tots = Hashtbl.create 17 in
  List.iter (fun act ->
    Hashtbl.add dead_acts act.act_guid act.name) cpt.dead_acts;
  Tree.iter (fun act -> 
    Hashtbl.add tots act.act_guid 0;
    (* Opening file descriptors and writing the headers *)
    let oc = open_out (sprintf "%s/%s.html" dirname act.act_guid) in
    header oc ~title:act.name;
    fprintf oc "<body>\n<table>\n";
    fprintf oc "<thead><th>%s</th><th>%s</th><th>%s</th>"
      "Date" "Description" "Memo";
    fprintf oc "<th>%s</th><th>%s</th></thead>\n"
      "Virement" "Montant";
    Hashtbl.add accounts act.act_guid (act.name, oc)) cpt.accounts;
  let transactions = List.fast_sort Pervasives.compare cpt.transactions in
  List.iter (write_tr accounts dead_acts tots) transactions;
  let big_tot = ref 0 in
  Hashtbl.iter (fun guid price ->
    big_tot := !big_tot - price;
    let oc = snd (Hashtbl.find accounts guid) in
    fprintf oc "<tr style=\"font-weight: bold;\"><td></td><td>Total</td>";
    fprintf oc "<td></td><td></td>";
    fprintf oc "<td>%d,%.2d</td></tr>\n" (-price/100) (-price%100))
    tots;
  Hashtbl.iter (fun _ (_, oc) ->
    fprintf oc "</table>\n</body>\n</html>\n";
    close_out oc) accounts;
  fprintf index_oc "<h3>Total</h3>\n<p>%d,%.2d</p>\n"
    (!big_tot/100) (!big_tot%100);
  fprintf index_oc "</body>\n</html>";
  close_out index_oc

(** {1 Sqlite3 output } *)

(** Initialisations of tables *)
let db_init db = 
  exec db ("CREATE TABLE IF NOT EXISTS accounts(" ^
    "guid TEXT(100) PRIMARY KEY, " ^
    "name TEXT, " ^
    "parent TEXT(100))")
    |> ignore ; 
  exec db ("CREATE TABLE IF NOT EXISTS dead_accounts(" ^
    "guid TEXT(100) PRIMARY KEY, " ^
    "name TEXT, " ^
    "parent TEXT(100))")
    |> ignore ;
  exec db ("CREATE TABLE IF NOT EXISTS transactions(" ^
    "guid TEXT(100) PRIMARY KEY, " ^
    "date DATE, " ^
    "description TEXT)")
    |> ignore ;
  exec db ("CREATE TABLE IF NOT EXISTS splits(" ^
    "id INTEGER PRIMARY KEY AUTOINCREMENT, " ^
    "act_guid TEXT, " ^
    "tr_id TEXT(100), " ^
    "price INTEGER, " ^
    "memo TEXT)")
    |> ignore 

(** Build a function to insert accounts and a function that has to be
 * called before closing the database to end up a connexion created in this
 * purpose. *)
let mk_insert_act db tbl =
  let stmt = prepare db @@ 
    sprintf "INSERT INTO %s(guid, name, parent) VALUES(?,?,?)" tbl in
  (
    begin fun act ->
      let _ = reset stmt in
      let _ = bind stmt 1 @@ Data.TEXT act.act_guid in
      let _ = bind stmt 2 @@ Data.TEXT act.name in
      let _ = bind stmt 3 @@ Data.TEXT act.parent in
      ignore @@ step stmt end
    ,
    fun () -> ignore @@ finalize stmt
  )

(** Build a function to insert splits and a function that has to be
 * called before closing the database to end up a connexion created in this
 * purpose. *)
let mk_insert_split db =
  let stmt = prepare db
    "INSERT INTO splits(act_guid, tr_id, price, memo) VALUES(?,?,?,?)" in
  (
    begin fun tr_id (act_guid, price, memo) ->
      let _ = reset stmt in
      let _ = bind stmt 1 @@ Data.TEXT act_guid in
      let _ = bind stmt 2 @@ Data.TEXT tr_id in
      let _ = bind stmt 3 @@ Data.INT (Int64.of_int price) in
      let _ = bind stmt 4 @@ Data.TEXT memo in
      ignore @@ step stmt end
    ,
    fun () -> ignore @@ finalize stmt
  )

(** Build a function to insert transactions and a function that has to be
 * called before closing the database to end up a connexion created in this
 * purpose. *)
let mk_insert_trn db =
  let stmt = prepare db
    "INSERT INTO transactions(guid, date, description) VALUES(?,?,?)" in
  (
    begin fun trn -> 
      let _ = reset stmt in
      let _ = bind stmt 1 @@ Data.TEXT trn.guid in
      let _ = bind stmt 2 @@ Data.TEXT trn.date in
      let _ = bind stmt 3 @@ Data.TEXT trn.description in
      ignore @@ step stmt end
    ,
    fun () -> ignore @@ finalize stmt
  )

let write_sqlite dbname cpt = 
  if Sys.file_exists dbname then begin
    eprintf "File %s already exists, aborting.\n" dbname;
    exit 1
  end;
  (* Connecting to the database *)
  let db = db_open dbname in
  db_init db;
  (* Inserting the accounts into the database *)
  let insert_act, end_insert_act = mk_insert_act db "accounts" in
  let insert_dead, end_insert_dead = mk_insert_act db "dead_accounts" in
  Tree.iter insert_act cpt.accounts;
  List.iter insert_dead cpt.dead_acts;
  end_insert_act () ; end_insert_dead () ;
  (* Inserting the transactions into the database *)
  let insert_trn, end_insert_trn = mk_insert_trn db in
  let insert_split, end_insert_split = mk_insert_split db in
  let add_trn trn =
    insert_trn trn;
    List.iter (insert_split trn.guid) trn.splits in
  List.iter add_trn cpt.transactions;
  end_insert_trn () ; end_insert_split () ;
  (* Ending up the connexion *)
  ignore @@ db_close db

