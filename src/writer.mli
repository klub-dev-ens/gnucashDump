(** Module for writing gnucash data *)

(** {1 HTML output } *)

(** Produces a set of HTML pages showing the whole book *)
val write_html : string -> Types.compta -> unit

(** {1 Sqlite3 output } *)

(** Produces a sqlite3 database containing the whole book.
{b NOT IMPLEMENTED YET} *)
val write_sqlite : string -> Types.compta -> unit
